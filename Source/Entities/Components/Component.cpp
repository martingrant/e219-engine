//
//  Component.cpp
//  E219 Prototype
//
//  Created by Marty on 07/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "Component.h"

#pragma region Class Constructor & Destructor
/*
 * Destructs the Component class.
 */
Component::~Component()
{
}

#pragma endregion

ComponentTypes Component::getComponentType()
{
    return m_componentType;
}
