//
//  PhysicalComponent.h
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <memory>

#include "Component.h"
#include "ComponentTypes.h"
#include "../../Input/Input.h"

class PlayerComponent : public Component
{
    /* Class Constructors & Destructors */
public:
	PlayerComponent();
	virtual ~PlayerComponent();
    
	/* General Public Methods*/
public:
    virtual void update();
    void getUserInput(std::shared_ptr<Input> input);
    
    virtual void receiveMessage(std::string message);
    virtual std::string sendMessage();
    
private:
    std::shared_ptr<Input> m_input;

};