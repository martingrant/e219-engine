//
//  Component.h
//  E219 Prototype
//
//  Created by Marty on 07/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <string>

#include "ComponentTypes.h"

class Entity;

class Component
{
    /* Class Constructor & Destructor */
public:
    virtual ~Component() = 0;
    
	/* General Public Methods*/
public:
	virtual void update() = 0;
    ComponentTypes getComponentType();
    virtual void receiveMessage(std::string message) = 0; // todo could maybe be implemented here
    virtual std::string sendMessage() = 0;

	void setOwner(Entity* owner) { m_owner = owner; }
	void assign() { m_assigned = true; }
	void unassign() { m_assigned = false; }
	bool isAssigned() { return m_assigned; }

protected:
	ComponentTypes m_componentType;
    std::string m_messageToSend;
	Entity* m_owner;
	bool m_assigned;
};