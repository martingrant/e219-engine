#pragma once 

enum ComponentTypes {
	PHYSICAL,
	MODEL,
	TEXTURE,
    PLAYER,
    BOUNDINGBOX,
	HEALTH,
};