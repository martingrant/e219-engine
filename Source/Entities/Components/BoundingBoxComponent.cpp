//
//  BoundingBoxComponent.cpp
//  E219 Engine
//
//  Created by Marty on 24/05/2015.
//  Copyright (c) 2015 Martin Grant. All rights reserved.
//

#include "BoundingBoxComponent.h"

BoundingBoxComponent::BoundingBoxComponent()
{
    
}


BoundingBoxComponent::~BoundingBoxComponent()
{
    
}


void BoundingBoxComponent::update()
{
    
}


void BoundingBoxComponent::receiveMessage(std::string message)
{
   if (message[0] == 'B' && message[1] == 'B')
   {
       // todo this definitely needs done a whole lot better!
       std::string x = message.substr(3, message.find("Y") - 3);
       std::string y = message.substr(message.find("Y") + 1, 8);
       std::string z = message.substr(message.find("Z") + 1, message.size());
       
       m_center.x = std::stof(x);
       m_center.y = std::stof(y);
       m_center.z = std::stof(z);
   }
}


std::string BoundingBoxComponent::sendMessage()
{
    return m_messageToSend;
}


void BoundingBoxComponent::setProperties(glm::vec3 center, glm::vec3 halfWidths)
{
    m_center = center;
    m_halfWidths = halfWidths;
}


glm::vec3 BoundingBoxComponent::getCenter()
{
    return m_center;
}


glm::vec3 BoundingBoxComponent::getHalfWidths()
{
    return m_halfWidths;
}