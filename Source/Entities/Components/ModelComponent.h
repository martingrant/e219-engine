//
//  PhysicalComponent.h
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <string>

#include "Component.h"
#include "ComponentTypes.h"

class ModelComponent : public Component
{
    /* Class Constructors & Destructors */
public:
	ModelComponent();
	virtual ~ModelComponent();
    
	/* General Public Methods*/
public:
	virtual void update();
    
    virtual void receiveMessage(std::string message);
    virtual std::string sendMessage();

	void setModelName(std::string modelName);
	std::string getModelName();

private:
	std::string m_modelName;
};