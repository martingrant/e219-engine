//
//  ComponentFactory.h
//  E219 Prototype
//
//  Created by Marty on 09/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <vector>
#include <memory>
#include <iostream>
#include <unordered_map>

#include "ComponentTypes.h"
#include "Component.h"
#include "PhysicalComponent.h"
#include "ModelComponent.h"
#include "TextureComponent.h"
#include "PlayerComponent.h"
#include "BoundingBoxComponent.h"
#include "HealthComponent.h"

class ComponentFactory
{
    /* Class Constructors & Destructors */
public:
	ComponentFactory();
    ~ComponentFactory();
    
    /* Component Management */
public:
	Component* orderNewComponent(ComponentTypes componentType);

	std::unordered_map<Component*, unsigned int> m_componentMap;
	std::vector<Component*> m_physicalComponentVector;
	std::vector<Component*> m_modelComponentVector;
	std::vector<Component*> m_textureComponentVector;
	std::vector<Component*> m_playerComponentVector;
	std::vector<Component*> m_boundingBoxComponentVector;
	std::vector<Component*> m_healthComponentVector;
    
private:
    unsigned int m_maximumComponents;

};