//
//  PhysicalComponent.cpp
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "ModelComponent.h"

#pragma region Class Constructors & Destructors
/*
 * Constructs the PhysicalComponent class.
 *
 * @param unsigned int componentID - integer ID used to identify the Component.
 */
ModelComponent::ModelComponent()
{
	m_componentType = MODEL;
}

/*
 * Destructs the PhysicalComponent class.
 */
ModelComponent::~ModelComponent()
{
    
}

#pragma endregion

#pragma region General Public Methods

void ModelComponent::update()
{

}


void ModelComponent::receiveMessage(std::string message)
{
    
}


std::string ModelComponent::sendMessage()
{
    return m_messageToSend;
}

#pragma endregion


void ModelComponent::setModelName(std::string modelName)
{
	m_modelName = modelName;
}


std::string ModelComponent::getModelName()
{
	return m_modelName;
}