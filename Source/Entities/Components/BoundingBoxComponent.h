//
//  BoundingBoxComponent.h
//  E219 Engine
//
//  Created by Marty on 24/05/2015.
//  Copyright (c) 2015 Martin Grant. All rights reserved.
//

#pragma once

#include <iostream>

#ifdef _WIN32
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#elif __APPLE__
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#endif

#include "Component.h"

class BoundingBoxComponent : public Component
{
    /* Class Constructors & Destructors */
public:
    BoundingBoxComponent();
    virtual ~BoundingBoxComponent();
    
    /* General Public Methods*/
public:
    virtual void update();
    
    virtual void receiveMessage(std::string message);
    virtual std::string sendMessage();
    
public:
    void setProperties(glm::vec3 center, glm::vec3 halfWidths);
    glm::vec3 getCenter();
    glm::vec3 getHalfWidths();
    
private:
    glm::vec3 m_center;
    glm::vec3 m_halfWidths;
};




