//
//  PhysicalComponent.cpp
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "PlayerComponent.h"

#pragma region Class Constructors & Destructors
/*
 * Constructs the PhysicalComponent class.
 *
 * @param unsigned int componentID - integer ID used to identify the Component.
 */
PlayerComponent::PlayerComponent()
{
	m_componentType = PLAYER;
}

/*
 * Destructs the PhysicalComponent class.
 */
PlayerComponent::~PlayerComponent()
{
    
}

#pragma endregion

#pragma region General Public Methods

void PlayerComponent::update()
{
    if (m_input->getKeyState("W")) m_messageToSend = "KW";
    else
    if (m_input->getKeyState("S")) m_messageToSend = "KS";
    else
    if (m_input->getKeyState("A")) m_messageToSend = "KA";
    else
    if (m_input->getKeyState("D")) m_messageToSend = "KD";
    else 
        m_messageToSend = "";
}


void PlayerComponent::getUserInput(std::shared_ptr<Input> input)
{
    m_input = input;
}


void PlayerComponent::receiveMessage(std::string message)
{
    m_messageToSend = message;
}


std::string PlayerComponent::sendMessage()
{
    return m_messageToSend;
}

#pragma endregion
