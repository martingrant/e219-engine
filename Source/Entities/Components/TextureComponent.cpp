//
//  PhysicalComponent.cpp
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "TextureComponent.h"

#pragma region Class Constructors & Destructors
/*
 * Constructs the PhysicalComponent class.
 *
 * @param unsigned int componentID - integer ID used to identify the Component.
 */
TextureComponent::TextureComponent()
{
	m_componentType = TEXTURE;
}

/*
 * Destructs the PhysicalComponent class.
 */
TextureComponent::~TextureComponent()
{
    
}

#pragma endregion

#pragma region General Public Methods

void TextureComponent::update()
{

}


void TextureComponent::receiveMessage(std::string message)
{
    
}


std::string TextureComponent::sendMessage()
{
    return m_messageToSend;
}

#pragma endregion


void TextureComponent::setTextureName(std::string textureName)
{
	m_textureName = textureName;
}


std::string TextureComponent::getTextureName()
{
	return m_textureName;
}