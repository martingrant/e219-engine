//
//  PhysicalComponent.h
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <string>
#include <iostream>

#ifdef _WIN32
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#elif __APPLE__
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#endif

#include "Component.h"
#include "ComponentTypes.h"


class PhysicalComponent : public Component
{
    /* Class Constructors & Destructors */
public:
	PhysicalComponent();
    virtual ~PhysicalComponent();
    
	/* General Public Methods*/
public:
	virtual void update();
    
    virtual void receiveMessage(std::string message);
    virtual std::string sendMessage();

public:
    /* Physical Property Methods */
    void setPosition(glm::vec3 newPosition);
	void setPositionX(float newPositionX);
	void setPositionY(float newPositionY);
	void setPositionZ(float newPositionZ);

	glm::vec3 getPosition();

    void setScale(glm::vec3 newScale);
	void setScaleX(float newScaleX);
	void setScaleZ(float newScaleY);
	void setScaleY(float newScaleZ);

	glm::vec3 getScale();
    
	void setRotationX(float newRotationX);
	void setRotationZ(float newRotationY);
	void setRotationY(float newRotationZ);

	glm::vec3 getRotation();
    
    void setHeading(float newHeading);
    float getHeading();

private:
	glm::vec3 m_position;
	glm::vec3 m_scale;
	glm::vec3 m_rotation;
    
    float m_acceleration;
    float m_velocity;
    
    float m_heading;
};