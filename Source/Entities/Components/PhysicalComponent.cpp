//
//  PhysicalComponent.cpp
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "PhysicalComponent.h"

#pragma region Class Constructors & Destructors
/*
 * Constructs the PhysicalComponent class.
 *
 * @param unsigned int componentID - integer ID used to identify the Component.
 */
PhysicalComponent::PhysicalComponent() : m_heading(0.0f), m_velocity(0.0f), m_acceleration(0.1f)
{
    m_componentType = PHYSICAL;
}

/*
 * Destructs the PhysicalComponent class.
 */
PhysicalComponent::~PhysicalComponent()
{
    
}

#pragma endregion

#pragma region General Public Methods

void PhysicalComponent::update()
{

}
#define DEG_TO_RADIAN 0.017453293

void PhysicalComponent::receiveMessage(std::string message)
{
	/*
    static bool colliding = false;
    
    if (message[0] == 'C')
    {
        colliding = true;
    }
    
    if (message[0] == 'K')
    {
		if (message[1] == 'W')
		{
			//setPositionZ(m_position.z - 0.1f);
			//m_position.x += (float)(0.01f * std::cos((m_heading - 90) * DEG_TO_RADIAN));
			//m_position.z -= (float)(0.01f * std::sin((m_heading - 90) * DEG_TO_RADIAN));
			//m_position.x += (float)(m_speed * std::cos(m_heading - 1.5708));	// 1.5708 radians = 90 degrees
			//m_position.z -= (float)(m_speed * std::sin(m_heading - 1.5708));

           // m_velocity += m_acceleration;
			m_velocity += 0.1f;
		}

		if (message[1] == 'S')
		{
			//setPositionZ(m_position.z + 0.1f);

			//m_position.x -= (float)(m_speed * std::cos(m_heading - 1.5708));	// 1.5708 radians = 90 degrees
			//m_position.z -= (float)(-m_speed * std::sin(m_heading - 1.5708));
            
          //  m_acceleration -= m_acceleration;
			m_velocity -= 0.1f;
		}

		if (message[1] == 'A') 
		{
			//setPositionX(m_position.x - m_speed);
		}

		if (message[1] == 'D')
		{
			//setPositionX(m_position.x + m_speed);
		}

        // Send position to bounding box
        m_messageToSend = "BBX" + std::to_string(m_position.x) + "Y" + std::to_string(m_position.y) + "Z" + std::to_string(m_position.z);
    }
    else
    {
        /*if (m_acceleration > 0.0f)
            m_acceleration -= 0.1f;
        
        if (m_acceleration <= 0.0f)
            m_acceleration = 0.0f;*/
        /*
        if (m_velocity > 0.0f)
            m_velocity -= 0.03125f;
        
        if (m_velocity <= 0.0f)
            m_velocity = 0.0f;
    }
    /*
    if (colliding)
    {
        m_velocity = 0.0f;
        m_acceleration = 0.0f;

		m_position.z += 1.0f;

		colliding = false;
    }
    else
    {
        m_acceleration = 0.1f;
    }*/
    
    
   // m_velocity += m_acceleration;
    /*
	m_position.x += (float)(m_velocity * std::cos(m_heading - 1.5708));	// 1.5708 radians = 90 degrees
    m_position.z -= (float)(m_velocity * std::sin(m_heading - 1.5708));
	*/
}


std::string PhysicalComponent::sendMessage()
{
    return m_messageToSend;
}

#pragma endregion

#pragma region Physical Property Methods

void PhysicalComponent::setPosition(glm::vec3 newPosition)
{
    m_position = newPosition;
}

/*
* Sets the X Position.
*
* @param float newPositionX - the new X Position.
*/
void PhysicalComponent::setPositionX(float newPositionX)
{
	m_position.x = newPositionX;
}

/*
 * Sets the Y Position.
 *
 * @param float newPositionY - the new Y Position.
 */
void PhysicalComponent::setPositionY(float newPositionY)
{
	m_position.y = newPositionY;
}

/*
* Sets the Z Position.
*
* @param float newPositionY - the new Z Position.
*/
void PhysicalComponent::setPositionZ(float newPositionZ)
{
	m_position.z = newPositionZ;
}

/*
* Returns Position.
*
* @return glm::vec3 - the Position.
*/
glm::vec3 PhysicalComponent::getPosition()
{
	return m_position;
}


void PhysicalComponent::setScale(glm::vec3 newScale)
{
    m_scale = newScale;
}


/*
* Sets the X Scale.
*
* @param float newScaleX - the new X scale.
*/
void PhysicalComponent::setScaleX(float newScaleX)
{
	m_scale.x = newScaleX;
}

/*
* Sets the Y Scale.
*
* @param float newScaleX - the new Y scale.
*/
void PhysicalComponent::setScaleY(float newScaleY)
{
	m_scale.y = newScaleY;
}

/*
* Sets the Z Scale.
*
* @param float newScaleZ - the new Z scale.
*/
void PhysicalComponent::setScaleZ(float newScaleZ)
{
	m_scale.z = newScaleZ;
}

/*
* Returns scale.
*
* @return glm::vec3 - the scale.
*/
glm::vec3 PhysicalComponent::getScale()
{
	return m_scale;
}

/*
* Sets the X rotation.
*
* @param float newRotationX - the new X rotation.
*/
void PhysicalComponent::setRotationX(float newRotationX)
{
	m_rotation.x = newRotationX;
}

/*
* Sets the Y rotation.
*
* @param float newRotationX - the new Y rotation.
*/
void PhysicalComponent::setRotationY(float newRotationY)
{
	m_rotation.y = newRotationY;
}


/*
* Sets the Z rotation.
*
* @param float newRotationZ - the new Z rotation.
*/
void PhysicalComponent::setRotationZ(float newRotationZ)
{
	m_rotation.z = newRotationZ;
}

/*
* Returns rotation.
*
* @return glm::vec3 - the rotation.
*/
glm::vec3 PhysicalComponent::getRotation()
{
	return m_rotation;
}


void PhysicalComponent::setHeading(float newHeading)
{
    m_heading = newHeading;
}


float PhysicalComponent::getHeading()
{
    return m_heading;
}


//void PhysicalComponent::rotateModelMatrix()
//{
//	// todo needs to be thought out
//	//m_modelMatrix = glm::rotate(m_rotate, )
//}


#pragma endregion



