//
//  ComponentFactory.cpp
//  E219 Prototype
//
//  Created by Marty on 09/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "ComponentFactory.h"

ComponentFactory::ComponentFactory()
{
	std::cout << "System constructed: ComponentFactory." << std::endl << std::endl;

    m_maximumComponents = 10;
    
	for (unsigned int index = 0; index < m_maximumComponents; ++index)
	{
		m_physicalComponentVector.emplace_back(new PhysicalComponent());
		m_modelComponentVector.emplace_back(new ModelComponent());
		m_textureComponentVector.emplace_back(new TextureComponent());
		m_playerComponentVector.emplace_back(new PlayerComponent());
		m_boundingBoxComponentVector.emplace_back(new BoundingBoxComponent());
		m_healthComponentVector.emplace_back(new HealthComponent());
	}
	int i = 1;
}


ComponentFactory::~ComponentFactory()
{
	std::cout << "System destructing: ComponentFactory." << std::endl << std::endl;
    
    for (unsigned int index = 0; index < m_maximumComponents; ++index)
    {
        delete m_physicalComponentVector[index];
        delete m_modelComponentVector[index];
        delete m_textureComponentVector[index];
        delete m_playerComponentVector[index];
        delete m_boundingBoxComponentVector[index];
        delete m_healthComponentVector[index];
    }
    
    m_physicalComponentVector.clear();
    m_modelComponentVector.clear();
    m_textureComponentVector.clear();
    m_playerComponentVector.clear();
    m_boundingBoxComponentVector.clear();
    m_healthComponentVector.clear();
}


Component* ComponentFactory::orderNewComponent(ComponentTypes componentType)
{
	switch (componentType)
	{
	case PHYSICAL:
		for (unsigned int index = 0; index < m_physicalComponentVector.size(); ++index)
		{
			if (m_physicalComponentVector[index]->isAssigned() == false)
			{
				m_physicalComponentVector[index]->assign();
				return m_physicalComponentVector[index];
			}
		}
		break;
	case MODEL:
		for (unsigned int index = 0; index < m_modelComponentVector.size(); ++index)
		{
			if (m_modelComponentVector[index]->isAssigned() == false)
			{
				m_modelComponentVector[index]->assign();
				return m_modelComponentVector[index];
			}
		}
		break;
	case TEXTURE:
		for (unsigned int index = 0; index < m_textureComponentVector.size(); ++index)
		{
			if (m_textureComponentVector[index]->isAssigned() == false)
			{
				m_textureComponentVector[index]->assign();
				return m_textureComponentVector[index];
			}
		}
		break;
	case PLAYER:
		for (unsigned int index = 0; index < m_playerComponentVector.size(); ++index)
		{
			if (m_playerComponentVector[index]->isAssigned() == false)
			{
				m_playerComponentVector[index]->assign();
				return m_playerComponentVector[index];
			}
		}
		break;
	case BOUNDINGBOX:
		for (unsigned int index = 0; index < m_boundingBoxComponentVector.size(); ++index)
		{
			if (m_boundingBoxComponentVector[index]->isAssigned() == false)
			{
				m_boundingBoxComponentVector[index]->assign();
				return m_boundingBoxComponentVector[index];
			}
		}
		break;
	case HEALTH:
		for (unsigned int index = 0; index < m_healthComponentVector.size(); ++index)
		{
			if (m_healthComponentVector[index]->isAssigned() == false)
			{
				m_healthComponentVector[index]->assign();
				return m_healthComponentVector[index];
			}
		}
		break;
	}
    return nullptr;
}