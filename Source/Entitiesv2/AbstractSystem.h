//
//  AbstractSystem.h
//  SpaceGame2D
//
//  Created by Marty on 16/11/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#pragma once

class AbstractSystem
{
public:
    virtual ~AbstractSystem() { };
    
public:
    virtual void update() = 0;
    virtual unsigned int orderNewComponent() = 0;
    
protected:
    unsigned int m_maximumComponents;
};