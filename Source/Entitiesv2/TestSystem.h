//
//  TestSystem.hpp
//  SpaceGame2D
//
//  Created by Marty on 16/11/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#pragma once

#include <vector>

#include "AbstractSystem.h"
#include "TestComponent.h"

class TestSystem : public AbstractSystem
{
public:
    TestSystem(unsigned int maximumComponents);
    virtual ~TestSystem();
    
public:
    void test();
    void update();
    unsigned int orderNewComponent();
    
private:
    std::vector<TestComponent> m_componentVector;
    
};
