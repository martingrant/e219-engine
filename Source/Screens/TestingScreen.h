#pragma once

#define DEG_TO_RADIAN 0.017453293

#include <stack>
#include <sstream> 
#include <cmath>
#include <sstream>

#ifdef _WIN32
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#elif __APPLE__
#include <SDL2/SDL.h>
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#endif

#include "Screen.h"
#include "../Renderer/Renderer.h"
#include "../Input/SDLInput.h"
#include "../Utilities/UtilitiesManager.h"
#include "../Entities/Components/PhysicalComponent.h"
#include "../Entities/EntityManager.h"
#include "../GUI/GUIManager.h"
#include "../GUI/GUIElementTypes.h"

class TestingScreen : public Screen
{
	/* Constructors & Destructors */
public:
	TestingScreen(std::shared_ptr<Renderer> renderer);
	~TestingScreen();

	/* General Public Methods */
public:
    void checkCollisions();
	virtual void update();
	virtual void render();

private:
	std::shared_ptr<Renderer> m_renderer;

	EntityManager* m_entityManager;

	GUIManager* guiManager;
};

