#include "TestingScreen.h"

#pragma region Constructors & Destructors

TestingScreen::TestingScreen(std::shared_ptr<Renderer> renderer) : m_renderer(renderer)
{
    m_renderer->loadTexture("white", "../Resources/Textures/white.png");
	m_renderer->loadTexture("metal", "../Resources/Textures/metal.bmp");
    m_renderer->loadTexture("crosshair", "../Resources/Textures/crosshair3.png");
    m_renderer->loadTexture("texttexture", "../Resources/Textures/texttexture.png");
	m_renderer->loadTexture("button", "../Resources/Textures/button.png");
	m_renderer->loadTexture("red", "../Resources/Textures/red.png");
    m_renderer->loadModel("cube.obj", "../Resources/Models/cube.obj");
    m_renderer->loadModel("cubeui.obj", "../Resources/Models/cubeui.obj");
	m_renderer->loadModel("sphere", "../Resources/Models/sphere.obj");
	m_renderer->loadModel("enemy", "../Resources/Models/enemy.obj");

    
	m_entityManager = new EntityManager();

    // player entity
	m_entityManager->createEntity("player");
	m_entityManager->addComponentToEntityName("player", PHYSICAL);
	PhysicalComponent* physicalComponent = (PhysicalComponent*)m_entityManager->getEntity("player")->getComponent(PHYSICAL);
    physicalComponent->setPosition(glm::vec3(0.0f, 1.0f, 0.0f));
    physicalComponent->setScale(glm::vec3(0.1f, 0.1f, 0.1f));

	m_entityManager->addComponentToEntityName("player", MODEL);
	ModelComponent* modelComponent = (ModelComponent*)m_entityManager->getEntity("player")->getComponent(MODEL);
	modelComponent->setModelName("sphere");

	m_entityManager->addComponentToEntityName("player", TEXTURE);
	TextureComponent* textureComponent = (TextureComponent*)m_entityManager->getEntity("player")->getComponent(TEXTURE);
	textureComponent->setTextureName("metal");
    
    m_entityManager->addComponentToEntityName("player", PLAYER);
    PlayerComponent* playerComponent = (PlayerComponent*)m_entityManager->getEntity("player")->getComponent(PLAYER);
    playerComponent->getUserInput(UtilitiesManager::getInstance().getInputManager());
    
    m_entityManager->addComponentToEntityName("player", BOUNDINGBOX);
    

    
    // collision test entity
    m_entityManager->createEntity("box");
    m_entityManager->addComponentToEntityName("box", PHYSICAL);
    PhysicalComponent* physicalComponent2 = (PhysicalComponent*)m_entityManager->getEntity("box")->getComponent(PHYSICAL);
    physicalComponent2->setPosition(glm::vec3(0.0f, 1.0f, -5.0f));
    physicalComponent2->setScale(glm::vec3(1.0f, 1.0f, 1.0f));
    
    m_entityManager->addComponentToEntityName("box", MODEL);
    ModelComponent* modelComponent2 = (ModelComponent*)m_entityManager->getEntity("box")->getComponent(MODEL);
    modelComponent2->setModelName("cube.obj");
    
    m_entityManager->addComponentToEntityName("box", TEXTURE);
    TextureComponent* textureComponent2 = (TextureComponent*)m_entityManager->getEntity("box")->getComponent(TEXTURE);
    textureComponent2->setTextureName("white");
    
    m_entityManager->addComponentToEntityName("box", BOUNDINGBOX);
    
    
    m_entityManager->createEntity("box2");
    m_entityManager->addComponentToEntityName("box2", PHYSICAL);
    PhysicalComponent* physicalComponent3 = (PhysicalComponent*)m_entityManager->getEntity("box2")->getComponent(PHYSICAL);
    physicalComponent3->setPosition(glm::vec3(-5.0f, 1.0f, -8.0f));
    physicalComponent3->setScale(glm::vec3(1.0f, 3.0f, 1.0f));
    
    m_entityManager->addComponentToEntityName("box2", MODEL);
    ModelComponent* modelComponent3 = (ModelComponent*)m_entityManager->getEntity("box2")->getComponent(MODEL);
    modelComponent3->setModelName("cube.obj");
    
    m_entityManager->addComponentToEntityName("box2", TEXTURE);
    TextureComponent* textureComponent3 = (TextureComponent*)m_entityManager->getEntity("box2")->getComponent(TEXTURE);
    textureComponent3->setTextureName("metal");
    
    m_entityManager->addComponentToEntityName("box2", BOUNDINGBOX);






	// enemy
	m_entityManager->createEntity("enemy");
	m_entityManager->addComponentToEntityName("enemy", PHYSICAL);
	PhysicalComponent* physicalComponent4 = (PhysicalComponent*)m_entityManager->getEntity("enemy")->getComponent(PHYSICAL);
	physicalComponent4->setPosition(glm::vec3(0.0f, 0.0f, -8.0f));
	physicalComponent4->setScale(glm::vec3(0.25f, 0.25f, 0.25f));

	m_entityManager->addComponentToEntityName("enemy", MODEL);
	ModelComponent* modelComponent4 = (ModelComponent*)m_entityManager->getEntity("enemy")->getComponent(MODEL);
	modelComponent4->setModelName("enemy");

	m_entityManager->addComponentToEntityName("enemy", TEXTURE);
	TextureComponent* textureComponent4 = (TextureComponent*)m_entityManager->getEntity("enemy")->getComponent(TEXTURE);
	textureComponent4->setTextureName("red");

	m_entityManager->addComponentToEntityName("enemy", BOUNDINGBOX);


	m_renderer->setCamera(FIRSTPERSON);
	m_renderer->setCameraTarget(m_entityManager->getEntity("player"));

	guiManager = new GUIManager();

	guiManager->createElement("menubutton", BUTTON, "button", 10, 10, 80, 80);
}


TestingScreen::~TestingScreen()
{
    
}

#pragma endregion

#pragma region General Public Methods


bool testAABBAABB(BoundingBoxComponent* a, BoundingBoxComponent* b)
{
    if ( glm::abs(a->getCenter().x - b->getCenter().x) > (a->getHalfWidths().x + b->getHalfWidths().x) ) return false;
    if ( glm::abs(a->getCenter().y - b->getCenter().y) > (a->getHalfWidths().y + b->getHalfWidths().y) ) return false;
    if ( glm::abs(a->getCenter().z - b->getCenter().z) > (a->getHalfWidths().z + b->getHalfWidths().z) ) return false;
    
    // We have an overlap
    return true;
};


void TestingScreen::checkCollisions()
{
    BoundingBoxComponent* bbox1 = (BoundingBoxComponent*)m_entityManager->getEntity("player")->getComponent(PHYSICAL);
    
    for (unsigned int index = 0; index < m_entityManager->getNumberOfEntities(); ++index)
    {
        if (index != 1)
        {
            if (m_entityManager->getEntity(index)->isAssigned() == true )
            {
                BoundingBoxComponent* bbox2 = (BoundingBoxComponent*)m_entityManager->getEntity(index)->getComponent(PHYSICAL);
                
                if (testAABBAABB(bbox1, bbox2))
                {
                    std::cout << "collision!" << std::endl;
                    m_entityManager->getEntity("player")->sendMessageToComponents("C");
                }
            }
            
        }
    }
}


void TestingScreen::update()
{
	m_renderer->updateCamera();

	if (UtilitiesManager::getInstance().getInputManager()->getKeyState("F1"))
	{
		m_renderer->setCamera(FIRSTPERSON);
	}
	
	if (UtilitiesManager::getInstance().getInputManager()->getKeyState("F2"))
	{
		m_renderer->setCamera(FREELOOK);
	}


    m_entityManager->update();
    
    checkCollisions();
    
	guiManager->update();
    
    if (guiManager->getGUIElement("menubutton")->isActive()) UtilitiesManager::getInstance().getWindowManager()->setCurrentScreen("MenuScreen");
}


void TestingScreen::render()
{
	for (int pass = 0; pass < 2; ++pass)
	{
		if (pass == 0)
		{
			// draw to framebuffer
			//glBindFramebuffer(GL_FRAMEBUFFER, m_renderer->FramebufferName);
			//glViewport(0, 0, 640, 480);

			//m_renderer->renderToTexture();

			m_renderer->renderDepth();
		}

		if (pass == 1)
		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}


		m_renderer->renderSkybox();

		m_renderer->useShaderProgram("textured");

		// m_renderer->renderProp("cube.obj", "white", 0.0f, 0.0f, 0.0f, 50.0f, 0.1f, 50.0f);

		m_renderer->renderEntity(m_entityManager->getEntity("player"));
		m_renderer->renderEntity(m_entityManager->getEntity("box"));
		m_renderer->renderEntity(m_entityManager->getEntity("box2"));
		m_renderer->renderEntity(m_entityManager->getEntity("enemy"));

		// ground
		m_renderer->renderPropLight("cube.obj", "white", 0.0f, 0.0f, 0.0f, 50.0f, 0.1f, 50.0f);
		// box
		m_renderer->renderPropLight("cube.obj", "white", 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);


		// test shadow

		m_renderer->renderProp("cube.obj", "white", 0.0f, 0.0f, 0.0f, 50.0f, 0.1f, 50.0f);



		m_renderer->useShaderProgram("textured");

		m_renderer->renderCrosshair();

		m_renderer->renderTexture("texttexture", 300.0f, 100.0f, 200.0f, 100.0f);
		m_renderer->renderTexture(guiManager->getGUIElement("menubutton")->getTexture(), guiManager->getGUIElement("menubutton")->getPositionX(), guiManager->getGUIElement("menubutton")->getPositionY(), guiManager->getGUIElement("menubutton")->getWidth(), guiManager->getGUIElement("menubutton")->getHeight());

		//m_renderer->renderToScreen();
	}
}

#pragma endregion
