#pragma once

#include <iostream>
#include <unordered_map>
#include <vector>
#include <memory>

#ifdef _WIN32
#include <SDL.h>
#include <GL/glew.h>
#elif __APPLE__
#include <SDL2/SDL.h>
#include <OpenGL/gl3.h>
#endif
#include "WindowManager.h"
#include "Screen.h"
#include "TestingScreen.h"
#include "MenuScreen.h"
#include "../Utilities/UtilitiesManager.h"
#include "../Renderer/Renderer.h"

class SDLWindowManager : public WindowManager
{
	/* Class Constructors & Destructors */
public:
	SDLWindowManager();
	virtual ~SDLWindowManager();

	/* General Public Methods */
public:
	virtual bool update();
	virtual void render();

	/* Screen Management */
public:
	virtual void setCurrentScreen(std::string screen);

private:
	std::unordered_map<std::string, std::shared_ptr<Screen>> m_screenMap;

	/* Window Management */
public:
	virtual void createWindow();
	virtual void setWindowTitle(std::string title);
	virtual void setWindowSize(int width, int height);
	virtual void setWindowWidth(int width);
	virtual void setWindowHeight(int height);
	virtual void minimiseWindow();
	virtual void restoreWindow();
	virtual void maximiseWindow();
	virtual void setFullScreenMode();
	virtual void setWindowedMode();

private:
	SDL_GLContext m_GLContext;
	SDL_Window* m_window;
	unsigned int m_displayDeviceIndex;
    
    unsigned int m_openGLVersionMajor;
    unsigned int m_openGLVersionMinor;

	std::string m_windowTitle;
	unsigned int m_windowWidth;
	unsigned int m_windowHeight;
	
	SDL_DisplayMode m_displayMode;
	void setUpDisplayModeVector();
	std::vector<SDL_DisplayMode> m_displayModeVector;
    
    bool m_running;
    SDL_Event m_SDLEvent;

private:
	std::shared_ptr<Renderer> m_renderer;
};

