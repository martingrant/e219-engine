#pragma once

#include <memory>
#include <iostream>

#include "../Screens/WindowManager.h"
#include "../Input/Input.h"
#include "../Time/TimeManager.h"
#include "MessageLog.h"
#include "ConfigLoader.h"

class UtilitiesManager
{
public:
	/* Class Constructors & Destructors */
	UtilitiesManager();
	~UtilitiesManager();

	UtilitiesManager(UtilitiesManager const&) = delete;
	void operator=(UtilitiesManager const&) = delete;

	static UtilitiesManager& getInstance()
	{
		static UtilitiesManager instance; // Guaranteed to be destroyed.
										  // Instantiated on first use.
		return instance;
	}

public:
	/* Service Management */
	void registerService(std::shared_ptr<WindowManager> windowManager);
	std::shared_ptr<WindowManager> getWindowManager();

	void registerService(std::shared_ptr<Input> inputManager);
	std::shared_ptr<Input> getInputManager();

	void registerService(std::shared_ptr<TimeManager> timeManager);
	std::shared_ptr<TimeManager> getTimeManager();

	void registerService(std::shared_ptr<MessageLog> messageLog);
	std::shared_ptr<MessageLog> getMessageLog();

	void registerService(std::shared_ptr<ConfigLoader> configLoader);
	std::shared_ptr<ConfigLoader> getConfigLoader();

private:
	/* Service Members */
	std::shared_ptr<WindowManager> m_windowManager;
	std::shared_ptr<Input> m_inputManager;
	std::shared_ptr<TimeManager> m_timeManager;
	std::shared_ptr<MessageLog> m_messageLog;
	std::shared_ptr<ConfigLoader> m_configLoader;
};

