#include "FirstPersonCamera.h"

FirstPersonCamera::FirstPersonCamera()
{
	m_position = glm::vec3(0.0f, 0.0f, 3.0f);
	m_focus = glm::vec3(0.0f, 0.0f, -1.0f);
	m_orientation = glm::vec3(0.0f, 1.0f, 0.0f);

	m_rotation = 0.0f;

	m_horizontalAngle = 3.14f;
	m_verticalAngle = 0.0f;

	m_windowWidth = atoi(UtilitiesManager::getInstance().getConfigLoader()->getValue("windowWidth").c_str());
	m_windowHeight = atoi(UtilitiesManager::getInstance().getConfigLoader()->getValue("windowHeight").c_str());
}


FirstPersonCamera::~FirstPersonCamera(void)
{
}


void FirstPersonCamera::update()
{
	std::shared_ptr<Input> input = UtilitiesManager::getInstance().getInputManager();

    //PhysicalComponent* physicalComponent = (PhysicalComponent*)m_target->getComponent(PHYSICAL);
	//m_position = moveForward(physicalComponent->getPosition(), -20.0f, physicalComponent->getHeading());
	//m_focus = moveForward(physicalComponent->getPosition(), 0.0f, physicalComponent->getHeading());

    // horizontal angle : toward -Z, these angles are in radians?
	m_horizontalAngle = 3.14f;
    // vertical angle : 0, look at the horizon
	m_verticalAngle = 0.0f;

    float mouseSpeed = 0.005f;
   
    // Compute new orientation
	m_horizontalAngle += mouseSpeed * float((float)m_windowWidth / 2 - input->getMousePosition().first);
	m_verticalAngle += mouseSpeed * float((float)m_windowHeight / 2 - input->getMousePosition().second);
    
    // Direction : Spherical coordinates to Cartesian coordinates conversion
    m_focus = glm::vec3(cos(m_verticalAngle) * sin(m_horizontalAngle), sin(m_verticalAngle), cos(m_verticalAngle) * cos(m_horizontalAngle));
    
    // Right vector
    glm::vec3 right(sin(m_horizontalAngle - 3.14f / 2.0f), 0, cos(m_horizontalAngle - 3.14f / 2.0f));
    
    // Up vector : perpendicular to both direction and right
	m_orientation = glm::vec3(glm::cross(right, m_focus));

	PhysicalComponent* physicalComponent = (PhysicalComponent*)m_target->getComponent(PHYSICAL);
	m_position = physicalComponent->getPosition();

	//physicalComponent->setHeading(glm::degrees(m_horizontalAngle));
	physicalComponent->setHeading(m_horizontalAngle);

  //  GLfloat cameraSpeed = 0.05f;
  //  if (input->getKeyState("W"))
  //  {
  //      //m_position += cameraSpeed * m_focus;	// to move camera with look direction
		//m_position.z -= 0.01f;					// move camera only with 2 axis
  //  }
  //  
  //  if (input->getKeyState("S"))
  //  {
  //      //m_position -= cameraSpeed * m_focus;
		//m_position.z -= 0.01f;
  //  }
  //  
  //  if (input->getKeyState("A"))
  //  {
  //      //m_position -= glm::normalize(glm::cross(m_focus, m_orientation)) * cameraSpeed;
		//m_position.x -= 0.01f;
  //  }
  //  
  //  if (input->getKeyState("D"))
  //  {
  //      //m_position += glm::normalize(glm::cross(m_focus, m_orientation)) * cameraSpeed;
		//m_position.x += 0.01f;
  //  }
}