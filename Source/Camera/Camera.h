#pragma once

#define DEG_TO_RADIAN 0.017453293

#ifdef _WIN32
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#elif __APPLE__
#include <OpenGL/gl3.h>
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#endif

#include "../Entities/Entity.h"	

enum CameraType
{
	FIRSTPERSON,
	FREELOOK,
};

class Camera
{
public:
	Camera();
	virtual ~Camera() = 0;

public:
    virtual void update() = 0;
    
    void setPosition(glm::vec3 newPosition);
    void setFocus(glm::vec3 newFocus);
    void setOrientation(glm::vec3 newOrientation);

	glm::vec3 getPosition();
	glm::vec3 getFocus();
	glm::vec3 getOrientation();

	glm::vec3 moveForward(glm::vec3 pos, GLfloat d, float r) {
		return glm::vec3(pos.x + d*std::sin(r*DEG_TO_RADIAN), pos.y, pos.z - d*std::cos(r*DEG_TO_RADIAN));
	}

	glm::vec3 moveRight(glm::vec3 pos, GLfloat d, float r) {
		return glm::vec3(pos.x + d*std::cos(r*DEG_TO_RADIAN), pos.y, pos.z + d*std::sin(r*DEG_TO_RADIAN));
	}

	void setTarget(Entity* entity);

protected:
	glm::vec3 m_position;
	glm::vec3 m_focus;
	glm::vec3 m_orientation;

	float m_rotation;

	Entity* m_target;
};

