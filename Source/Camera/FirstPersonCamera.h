#pragma once

#define DEG_TO_RADIAN 0.017453293

#ifdef _WIN32
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#elif __APPLE__
#include <OpenGL/gl3.h>
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#endif

#include "Camera.h"
#include "../Utilities/UtilitiesManager.h"
#include "../Entities/Entity.h"
#include "../Entities/Components/PhysicalComponent.h"

class FirstPersonCamera : public Camera
{
public:
	FirstPersonCamera();
	virtual ~FirstPersonCamera();

public:
    virtual void update();

private:
	float m_horizontalAngle;
	float m_verticalAngle;

	unsigned int m_windowWidth;
	unsigned int m_windowHeight;
};

