#include "Camera.h"


Camera::Camera()
{
	m_position = glm::vec3(0.0f, 0.0f, 3.0f);
	m_focus = glm::vec3(0.0f, 0.0f, -1.0f);
	m_orientation = glm::vec3(0.0f, 1.0f, 0.0f);

	m_rotation = 0.0f;
}


Camera::~Camera(void)
{
}


void Camera::setPosition(glm::vec3 newPosition)
{
    m_position = newPosition;
}


void Camera::setFocus(glm::vec3 newFocus)
{
    m_focus = newFocus;
}


void Camera::setOrientation(glm::vec3 newOrientation)
{
    m_orientation = newOrientation;
}


glm::vec3 Camera::getPosition()
{
	return m_position;
}


glm::vec3 Camera::getFocus()
{
	return m_focus;
}


glm::vec3 Camera::getOrientation()
{
	return m_orientation;
}


void Camera::setTarget(Entity* entity)
{
	m_target = entity;
}
