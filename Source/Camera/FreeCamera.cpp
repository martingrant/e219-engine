#include "FreeCamera.h"

FreeCamera::FreeCamera()
{
	m_position = glm::vec3(0.0f, 0.0f, 3.0f);
	m_focus = glm::vec3(0.0f, 0.0f, -1.0f);
	m_orientation = glm::vec3(0.0f, 1.0f, 0.0f);

	m_rotation = 0.0f;
}


FreeCamera::~FreeCamera(void)
{
}


void FreeCamera::update()
{
	std::shared_ptr<Input> input = UtilitiesManager::getInstance().getInputManager();

    if (input->getKeyState("W"))
    {
		m_position.z -= 0.01f;					
    }
    
    if (input->getKeyState("S"))
    {
		m_position.z += 0.01f;
    }
    
    if (input->getKeyState("A"))
    {
		m_position.x -= 0.01f;
    }
    
    if (input->getKeyState("D"))
    {
		m_position.x += 0.01f;
    }

	if (input->getKeyState("R"))
	{
		m_position.y += 0.01f;
	}

	if (input->getKeyState("F"))
	{
		m_position.y -= 0.01f;
	}

	if (input->getKeyState("LEFT"))
	{
		m_focus = glm::rotate(m_focus, 1.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	}

	if (input->getKeyState("RIGHT"))
	{
		m_focus = glm::rotate(m_focus, -1.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	}

	if (input->getKeyState("UP"))
	{
		m_focus = glm::rotate(m_focus, 1.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	}

	if (input->getKeyState("DOWN"))
	{
		m_focus = glm::rotate(m_focus, -1.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	}
}
