//
//  Camera2D.cpp
//  SpaceGame2D
//
//  Created by Marty on 25/10/2015.
//  Copyright © 2015 Midnight Pacific. All rights reserved.
//

#include "Camera2D.h"

Camera2D::Camera2D() : m_viewMatrix(glm::mat4(1.0f)), m_positionX(0), m_positionY(0)
{
}


Camera2D::~Camera2D()
{
    
}


void Camera2D::update(std::shared_ptr<Input> input)
{
    m_viewMatrix = glm::mat4(1.0f);
    
    if (input->getKeyState("up"))
    {
        m_positionY += 10.0f;
    }
    if (input->getKeyState("down"))
    {
        m_positionY -= 10.0f;
    }
    if (input->getKeyState("left"))
    {
        m_positionX += 10.0f;
    }
    if (input->getKeyState("right"))
    {
        m_positionX -= 10.0f;
    }
    
    m_viewMatrix = glm::translate(m_viewMatrix, glm::vec3(m_positionX, m_positionY, 0.0f));
}


glm::mat4 Camera2D::getViewMatrix()
{
    return m_viewMatrix;
}