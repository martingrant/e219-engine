#pragma once

#define DEG_TO_RADIAN 0.017453293

#include <memory>

#ifdef _WIN32
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#elif __APPLE__
#include <OpenGL/gl3.h>
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#include "rotate_vector.hpp"
#endif

#include "Camera.h"
#include "../Utilities/UtilitiesManager.h"

class FreeCamera : public Camera
{
public:
	FreeCamera();
	virtual ~FreeCamera();

public:
    virtual void update();
    
};

