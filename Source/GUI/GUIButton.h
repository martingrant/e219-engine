#pragma once

#include <string>

#include "GUIElement.h"

class GUIButton : public GUIElement
{
public:
	GUIButton(std::string texture, float positionX, float positionY, float width, float height);
	~GUIButton();

private:

};

