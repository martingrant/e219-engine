#include "GUIElement.h"


GUIElement::~GUIElement()
{
}


bool GUIElement::mouseOverlap(float mouseX, float mouseY)
{
	if (mouseX > m_positionX && mouseX < m_positionX + m_width)
	{
		if (mouseY > m_positionY && mouseY < m_positionY + m_height)
		{
			return true;
		}
	}
	return false;
}


void GUIElement::activate()
{
    m_active = true;
}


void GUIElement::deactivate()
{
    m_active = false;
}


bool GUIElement::isActive()
{
    return m_active;
}


std::string GUIElement::getTexture()
{
	return m_texture;
}


float GUIElement::getPositionX()
{
	return m_positionX;
}


float GUIElement::getPositionY()
{
	return m_positionY;
}


float GUIElement::getWidth()
{
	return m_width;
}


float GUIElement::getHeight()
{
	return m_height;
}
