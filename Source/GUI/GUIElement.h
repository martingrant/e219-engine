#pragma once

#include <iostream>
#include <string>

class GUIElement
{
public:
	virtual ~GUIElement();

public:
	bool mouseOverlap(float mouseX, float mouseY);
    void activate();
    void deactivate();
    bool isActive();
	std::string getTexture();
	float getPositionX();
	float getPositionY();
	float getWidth();
	float getHeight();

protected:
	float m_positionX;
	float m_positionY;
	float m_width;
	float m_height;

	bool m_active;

	std::string m_texture;
};

