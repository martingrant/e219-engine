#include "Renderer.h"


Renderer::Renderer(unsigned int windowWidth, unsigned int windowHeight)
{
	UtilitiesManager::getInstance().getMessageLog()->logString("System constructing: Renderer");
    
    // Set up projection matrix
    // todo parameterise this properly
	setProjectionMatrix(60.0f, 800.0f, 600.0f, 1.0f, 150.0f);

	const char *cubeTexFiles[6] =
	{
		"../Resources/Textures/Skybox/back.bmp",
		"../Resources/Textures/Skybox/front.bmp",
		"../Resources/Textures/Skybox/right.bmp",
		"../Resources/Textures/Skybox/left.bmp",
		"../Resources/Textures/Skybox/top.bmp",
		"../Resources/Textures/Skybox/bottom.bmp",
	};
	loadCubeMap(cubeTexFiles);

	m_shaderProgramMap.emplace("currentShader", -1);
	loadShaders("../Source/Shaders");
	
	m_firstPersonCamera = new FirstPersonCamera();
	m_freeCamera = new FreeCamera();
	m_camera = m_freeCamera;

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE); // make sure depth test is on
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
    
	glClearColor(0.8f, 0.8f, 0.8f, 1.0f);

    UtilitiesManager::getInstance().getMessageLog()->logString("System constructed: Renderer");


	selectedLight = 0;
	// light object constructors
	lightList[0] = Light(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), glm::vec4(0.8f, 0.0f, 0.0f, 1.0f), glm::vec4(0.8f, 0.0f, 0.0f, 1.0f), glm::vec4(0.0f, 2.0f, 0.0f, 1.0f), glm::vec3(0.01, -1.41, 0.0), 1.39, glm::vec3(1.0f, 0.01f, 0.001f), 1, 1);
	lightList[1] = Light(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), glm::vec4(0.0f, 0.5f, 1.0f, 1.0f), glm::vec4(0.0f, 0.1f, 1.0f, 1.0f), glm::vec4(2.0f, 5.0f, 0.0f, 1.0f), glm::vec3(0.01, -1.41, 0.0), 1.39, glm::vec3(1.0f, 0.01f, 0.001f), 1, 1);

	// light position in world space
	lightPositions[0] = glm::vec4(0.0f, 2.0f, 0.0f, 1.0f);
	lightPositions[1] = glm::vec4(2.0f, 5.0f, 0.0f, 1.0f);


	// light cone for spotlights
	lightConeDirection[0] = glm::vec3(0.01, -1.41, 0.0);
	lightConeDirection[1] = glm::vec3(0.01, -1.41, 0.0);
}


Renderer::~Renderer()
{
    UtilitiesManager::getInstance().getMessageLog()->logString("System destructing: Renderer");
    
	// Delete shaders and clear map
	for (auto iterator = m_shaderMap.begin(); iterator != m_shaderMap.end(); ++iterator)
	{
		glDeleteShader(iterator->second);
	}
	m_shaderMap.clear();

	// Delete shader programs and clear map
	for (auto iterator = m_shaderProgramMap.begin(); iterator != m_shaderProgramMap.end(); ++iterator)
	{
		glDeleteProgram(iterator->second);
	}
	m_shaderProgramMap.clear();

	// Delete texture map and clear it
	for (auto iterator = m_textureMap.begin(); iterator != m_textureMap.end(); ++iterator)
	{
		glDeleteTextures(1, &iterator->second);
	}
	m_textureMap.clear();
    
    UtilitiesManager::getInstance().getMessageLog()->logString("System destructed: Renderer");
}


void Renderer::renderToTexture()
{
	FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	//GLuint renderedTexture;
	glGenTextures(1, &renderedTexture);

	glBindTexture(GL_TEXTURE_2D, renderedTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 640, 480, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	GLuint depthrenderbuffer;
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 640, 480);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);

	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);
}


void Renderer::renderToScreen()
{
	//GLuint quad_VertexArrayID;
	//glGenVertexArrays(1, &quad_VertexArrayID);
	//glBindVertexArray(quad_VertexArrayID);

	//static const GLfloat g_quad_vertex_buffer_data[] = {
	//	-1.0f, -1.0f, 0.0f,
	//	1.0f, -1.0f, 0.0f,
	//	-1.0f, 1.0f, 0.0f,
	//	-1.0f, 1.0f, 0.0f,
	//	1.0f, -1.0f, 0.0f,
	//	1.0f, 1.0f, 0.0f,
	//};

	//GLuint quad_vertexbuffer;
	//glGenBuffers(1, &quad_vertexbuffer);
	//glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);

	//// Create and compile our GLSL program from the shaders
	//GLuint quad_programID = LoadShaders("Passthrough.vertexshader", "SimpleTexture.fragmentshader");
	//GLuint texID = glGetUniformLocation(quad_programID, "renderedTexture");
	//GLuint timeID = glGetUniformLocation(quad_programID, "time");

	glDepthMask(GL_FALSE);
	useShaderProgram("ui");

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, renderedTexture);

	int screenwidth = 640;
	int screenheight = 480;

	int width = 200;
	int height = 200;
	int positionX = 100;
	int positionY = 100;

	glm::mat4 model = glm::mat4(1.0f);


	glm::vec3 translationVec = glm::vec3(-1.0f, 1.0f, 0.0f) +  // move quad center to the top left of the screen
		glm::vec3((GLfloat)width / (GLfloat)screenwidth, -((GLfloat)height / (GLfloat)screenheight), 0.0f) + // move quad so that it's fully inside the screen
		glm::vec3((GLfloat)positionX * 2 / (GLfloat)screenwidth, -((GLfloat)positionY * 2 / (GLfloat)screenheight), 0.0f); // move quad to it's final location
	model = glm::translate(model, translationVec); //  move it to it's final location (this happens after scale)
	//model = glm::rotate(model, -90.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3((GLfloat)width / (GLfloat)screenwidth, (GLfloat)height / (GLfloat)screenheight, 0.0f)); // scale to given size


	/*glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(positionX, positionY, 1.0f));
	model = glm::scale(model, glm::vec3(width, height, 0.0f));
	model = glm::rotate(model, 90.0f, glm::vec3(-1.0f, 0.0f, 0.0f));*/
	setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "model", glm::value_ptr(model));

	drawIndexedMesh(m_modelMap["cube.obj"].getVAOVector()[0], m_modelMap["cube.obj"].getMeshIndexCountVector()[0], GL_TRIANGLES);

	glDepthMask(GL_TRUE);
}


void Renderer::renderDepth()
{
	depthBuffer = 0;
	glGenFramebuffers(1, &depthBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, depthBuffer);

	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 640, 480, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

	glDrawBuffer(GL_NONE); // No color buffer is drawn to.
}


void Renderer::setUniform(GLuint shader, Light iLightList[])
{
	for (int i = 0; i < 2; i++) 
	{
		std::stringstream ss;
		ss << (i);
		std::string s = ss.str();
		std::string temp = "lights[" + s + "]" + ".ambient";
		int uniformIndex = glGetUniformLocation(shader, temp.c_str());
		glUniform4fv(uniformIndex, 1, glm::value_ptr(iLightList[i].getAmbient()));
		temp = "lights[" + s + "]" + ".diffuse";
		uniformIndex = glGetUniformLocation(shader, temp.c_str());
		glUniform4fv(uniformIndex, 1, glm::value_ptr(iLightList[i].getDiffuse()));
		temp = "lights[" + s + "]" + ".specular";
		uniformIndex = glGetUniformLocation(shader, temp.c_str());
		glUniform4fv(uniformIndex, 1, glm::value_ptr(iLightList[i].getSpecular()));
		temp = "lights[" + s + "]" + ".position";
		uniformIndex = glGetUniformLocation(shader, temp.c_str());
		glUniform4fv(uniformIndex, 1, glm::value_ptr(iLightList[i].getPosition()));
		temp = "lights[" + s + "]" + ".coneDirection";
		uniformIndex = glGetUniformLocation(shader, temp.c_str());
		glUniform3fv(uniformIndex, 1, glm::value_ptr(iLightList[i].getConeDirection()));
		temp = "lights[" + s + "]" + ".coneFallOff";
		uniformIndex = glGetUniformLocation(shader, temp.c_str());
		glUniform1f(uniformIndex, iLightList[i].getConeFallOff());
		temp = "lights[" + s + "]" + ".attenuation";
		uniformIndex = glGetUniformLocation(shader, temp.c_str());
		glUniform3fv(uniformIndex, 1, glm::value_ptr(iLightList[i].getAttenuation()));
		temp = "lights[" + s + "]" + ".IsActive";
		uniformIndex = glGetUniformLocation(shader, temp.c_str());
		glUniform1i(uniformIndex, iLightList[i].getStatus());
		temp = "lights[" + s + "]" + ".type";
		uniformIndex = glGetUniformLocation(shader, temp.c_str());
		glUniform1i(uniformIndex, iLightList[i].getType());
	}
}


void Renderer::setUniform3f(GLuint shader, std::string uniform, const GLfloat* data)
{
    int uniformIndex = glGetUniformLocation(shader, uniform.c_str());
    glUniform3f(uniformIndex, data[0], data[1], data[2]);
}


void Renderer::setUniform4f(GLuint shader, std::string uniform, const GLfloat* data)
{
	int uniformIndex = glGetUniformLocation(shader, uniform.c_str());
	glUniform4f(uniformIndex, data[0], data[1], data[2], data[3]);
}


void Renderer::setUniformMatrix3fv(const GLuint program, const char* uniformName, const GLfloat *data)
{
	int uniformIndex = glGetUniformLocation(program, uniformName);
	glUniformMatrix3fv(uniformIndex, 1, GL_FALSE, data);
}


void Renderer::setUniformMatrix4fv(const GLuint program, const char* uniformName, const GLfloat *data) {
	int uniformIndex = glGetUniformLocation(program, uniformName);
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, data);
}


void Renderer::drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive) {
	glBindVertexArray(mesh);	// Bind mesh VAO
	glDrawElements(primitive, indexCount, GL_UNSIGNED_INT, nullptr);	// draw VAO 
	glBindVertexArray(0);
}


void Renderer::renderBegin()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_stack.push(m_modelview);

    //m_stack.top() = glm::lookAt(m_camera->getPosition(), m_camera->getPosition() + m_camera->getFocus(), m_camera->getOrientation());
	//m_stack.top() = glm::lookAt(m_freeCamera->getPosition(), m_freeCamera->getPosition() + m_freeCamera->getFocus(), m_freeCamera->getOrientation());
	m_stack.top() = glm::lookAt(m_camera->getPosition(), m_camera->getPosition() + m_camera->getFocus(), m_camera->getOrientation());


	// Multiply light positions by modelview
	glm::vec4 tmp;
	glm::vec3 tmp2;
	glm::mat3 rotOnlyViewMatrix;
	for (int i = 0; i < 2; i++)
	{
		tmp = m_stack.top()*lightPositions[i];
		lightList[i].setPositionX(tmp.x);
		lightList[i].setPositionY(tmp.y);
		lightList[i].setPositionZ(tmp.z);

		rotOnlyViewMatrix = glm::mat3(m_stack.top());
		tmp2 = rotOnlyViewMatrix * glm::vec3(lightConeDirection[i].x, lightConeDirection[i].y, lightConeDirection[i].z); // passing cone direction or look at into eye coordinates
		lightList[i].setConeDirectionX(tmp2.x);
		lightList[i].setConeDirectionY(tmp2.y);
		lightList[i].setConeDirectionZ(tmp2.z);
	}
	useShaderProgram("multiplelight");
	setUniform(m_shaderProgramMap["currentShader"], lightList);
	setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "projection", glm::value_ptr(m_projectionMatrix));
	useShaderProgram("textured");
}


void Renderer::renderEnd()
{
    m_stack.pop();
}


void Renderer::renderEntity(Entity* entity)
{
	glActiveTexture(GL_TEXTURE0);

	TextureComponent* textureComponent = (TextureComponent*)entity->getComponent(TEXTURE);
	useTexture2D(textureComponent->getTextureName());

    setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "projection", glm::value_ptr(m_projectionMatrix));
    
	PhysicalComponent* physicalComponent = (PhysicalComponent*)entity->getComponent(PHYSICAL);

	ModelComponent* modelComponent = (ModelComponent*)entity->getComponent(MODEL);
	
    m_stack.push(m_stack.top());
    m_stack.top() = glm::translate(m_stack.top(), physicalComponent->getPosition());
    m_stack.top() = glm::scale(m_stack.top(), physicalComponent->getScale());

	setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "modelview", glm::value_ptr(m_stack.top()));

	unsigned int numberOfMeshes = m_modelMap[modelComponent->getModelName()].getNumberOfMeshes();

	for (unsigned int index = 0; index < numberOfMeshes; ++index)
	{
		drawIndexedMesh(m_modelMap[modelComponent->getModelName()].getVAOVector()[index], m_modelMap[modelComponent->getModelName()].getMeshIndexCountVector()[index], GL_TRIANGLES);
	}
    
    m_stack.pop();
}


void Renderer::renderProp(std::string modelName, std::string textureName, float xPosition, float yPosition, float zPosition, float xScale, float yScale, float zScale)
{
	glActiveTexture(GL_TEXTURE0);
	useTexture2D(textureName);

	setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "projection", glm::value_ptr(m_projectionMatrix));

    m_stack.push(m_stack.top());
    
	m_stack.top() = glm::translate(m_stack.top(), glm::vec3(xPosition, yPosition, zPosition));
	m_stack.top() = glm::scale(m_stack.top(), glm::vec3(xScale, yScale, zScale));
	setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "modelview", glm::value_ptr(m_stack.top()));

	unsigned int numberOfMeshes = m_modelMap["cube.obj"].getNumberOfMeshes();

	for (unsigned int index = 0; index < numberOfMeshes; ++index)
	{
		drawIndexedMesh(m_modelMap[modelName].getVAOVector()[index], m_modelMap[modelName].getMeshIndexCountVector()[index], GL_TRIANGLES);
	}
    
    m_stack.pop();
}


void Renderer::renderPropLight(std::string modelName, std::string textureName, float xPosition, float yPosition, float zPosition, float xScale, float yScale, float zScale)
{
    glActiveTexture(GL_TEXTURE0);
    useTexture2D(textureName);
    
    useShaderProgram("multiplelight");
    
    setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "projection", glm::value_ptr(m_projectionMatrix));
    
    m_stack.push(m_stack.top());
    
    m_stack.top() = glm::translate(m_stack.top(), glm::vec3(xPosition, yPosition, zPosition));
    m_stack.top() = glm::scale(m_stack.top(), glm::vec3(xScale, yScale, zScale));
    setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "modelview", glm::value_ptr(m_stack.top()));
    
    // normal matrix constructed on cpu
    glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(m_stack.top())));
    setUniformMatrix3fv(m_shaderProgramMap["currentShader"], "normalMatrix", glm::value_ptr(normalMatrix));
    
    unsigned int numberOfMeshes = m_modelMap["cube.obj"].getNumberOfMeshes();
    
    for (unsigned int index = 0; index < numberOfMeshes; ++index)
    {
        drawIndexedMesh(m_modelMap[modelName].getVAOVector()[index], m_modelMap[modelName].getMeshIndexCountVector()[index], GL_TRIANGLES);
    }
    
    m_stack.pop();
}


void Renderer::renderSkybox()
{
	useShaderProgram("cubemap");
	setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "projection", glm::value_ptr(m_projectionMatrix));

	glDepthMask(GL_FALSE); // make sure writing to update depth test is off
	glCullFace(GL_FRONT); // drawing inside of cube
	glActiveTexture(GL_TEXTURE0);
	useTexture2D("skyBox");

    glm::mat3 mvRotOnlyMat3 = glm::mat3(m_stack.top());
    m_stack.push(glm::mat4(mvRotOnlyMat3));
    
    glCullFace(GL_FRONT); // drawing inside of cube
    glActiveTexture(GL_TEXTURE0);
    useTexture2D("skyBox");
    m_stack.top() = glm::scale(m_stack.top(), glm::vec3(1.5f, 1.5f, 1.5f));
    setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "modelview", glm::value_ptr(m_stack.top()));
	drawIndexedMesh(m_modelMap["cube.obj"].getVAOVector()[0], m_modelMap["cube.obj"].getMeshIndexCountVector()[0], GL_TRIANGLES);

    m_stack.pop();
    
	glCullFace(GL_BACK);
	glDepthMask(GL_TRUE);
}


void Renderer::renderCrosshair()
{
    glDepthMask(GL_FALSE);
    useShaderProgram("ui");
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_textureMap["crosshair"]);
    
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.0f));
    model = glm::rotate(model, 90.0f, glm::vec3(-1.0f, 0.0f, 0.0f));
    setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "model", glm::value_ptr(model));
    
    drawIndexedMesh(m_modelMap["cube.obj"].getVAOVector()[0], m_modelMap["cube.obj"].getMeshIndexCountVector()[0], GL_TRIANGLES);
    
    glDepthMask(GL_TRUE);
}


void Renderer::renderTexture(std::string name, float positionX, float positionY, unsigned int width, unsigned int height)
{
	glDepthMask(GL_FALSE);
	useShaderProgram("ui");

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_textureMap[name]);

	int screenwidth = 640;
	int screenheight = 480;

	glm::mat4 model = glm::mat4(1.0f);


	glm::vec3 translationVec = glm::vec3(-1.0f, 1.0f, 0.0f) +  // move quad center to the top left of the screen
		glm::vec3((GLfloat)width / (GLfloat)screenwidth, -((GLfloat)height / (GLfloat)screenheight), 0.0f) + // move quad so that it's fully inside the screen
		glm::vec3((GLfloat)positionX * 2 / (GLfloat)screenwidth, -((GLfloat)positionY * 2 / (GLfloat)screenheight), 0.0f); // move quad to it's final location
	model = glm::translate(model, translationVec); //  move it to it's final location (this happens after scale)
	//model = glm::rotate(model, -90.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3((GLfloat)width / (GLfloat)screenwidth, (GLfloat)height / (GLfloat)screenheight, 0.0f)); // scale to given size


	/*glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(positionX, positionY, 1.0f));
	model = glm::scale(model, glm::vec3(width, height, 0.0f));
	model = glm::rotate(model, 90.0f, glm::vec3(-1.0f, 0.0f, 0.0f));*/
	setUniformMatrix4fv(m_shaderProgramMap["currentShader"], "model", glm::value_ptr(model));

	drawIndexedMesh(m_modelMap["cube.obj"].getVAOVector()[0], m_modelMap["cube.obj"].getMeshIndexCountVector()[0], GL_TRIANGLES);

	glDepthMask(GL_TRUE);
}


void Renderer::setProjectionMatrix(float fieldOfView, float aspectX, float aspectY, float nearPlane, float farPlane)
{
	m_projectionMatrix = glm::perspective(fieldOfView, aspectX / aspectY, nearPlane, farPlane);
}


void Renderer::setCameraTarget(Entity* entity)
{
	m_camera->setTarget(entity);
}


void Renderer::updateCamera()
{
    m_camera->update();
}


void Renderer::setCamera(CameraType cameraType)
{
	switch (cameraType)
	{
		case FIRSTPERSON:
			m_camera = m_firstPersonCamera;
			break;

		case FREELOOK:
			m_camera = m_freeCamera;
			break;
	}
}


void Renderer::useShaderProgram(std::string shaderName)
{
	bool shaderSet = false;

	for (auto iterator = m_shaderProgramMap.begin(); iterator != m_shaderProgramMap.end(); ++iterator)
	{
		if (iterator->first == shaderName)
		{
			glUseProgram(iterator->second);
			shaderSet = true;
			m_shaderProgramMap.at("currentShader") = m_shaderProgramMap[shaderName];
			//m_s->getMessageLog()->logString("Using shader '" + shaderName + "'.");
		}
	}

	if (shaderSet == false)
	{
		//UtilitiesManager::getInstance().getMessageLog()->logString("Could not use shader '" + shaderName + "'. Shader not found.");
	}
}


void Renderer::loadShaders(const char* directoryPath)
{
	std::vector<std::string> shaders;

    UtilitiesManager::getInstance().getMessageLog()->logString(std::string("Attempting to load shaders located in:") + directoryPath);
    
	DIR *directory;
	struct dirent *entry;
	if ((directory = opendir(directoryPath)) != NULL)
	{
		/* print all the files and directories within directory */
		while ((entry = readdir(directory)) != NULL)
		{
			std::string s = entry->d_name;

			if (s.size() > 5) // Some records come up as "." and "..", this will disqualify them. Shader filenames will be at least 6 characters in length e.g. 1.vert
			{
				if (s != ".DS_Store") // Get rid of this file on OS X
					shaders.push_back(s);
			}
		}
		closedir(directory);

		for (unsigned int index = 0; index < shaders.size() - 1; index += 2) // 2 linked shaders are typically named the same so will be listed together.. might cause problems in future though
		{
			size_t findDot = shaders[index].find(".");
			std::string shaderName = shaders[index].substr(0, findDot);

			std::string vertexShader = directoryPath;
			vertexShader += "/";
			vertexShader += shaders[index];

			std::string fragmentShader = directoryPath;
			fragmentShader += "/";
			fragmentShader += shaders[index + 1];

			loadShader(shaderName.c_str(), vertexShader.c_str(), fragmentShader.c_str());
		}
        UtilitiesManager::getInstance().getMessageLog()->logString(std::string("Loaded shaders located in: ") + directoryPath);
	}
	else
	{
        UtilitiesManager::getInstance().getMessageLog()->logString(std::string("Couldn't open directory: ") + directoryPath);
	}
}


char* Renderer::checkForShaderErrors(const GLuint shader)
{
	int maximumLength = 0;
	int messageLength = 0;

	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maximumLength);

	char* message = nullptr;

	if (maximumLength > 0)
	{
		message = new GLchar[maximumLength];
		glGetShaderInfoLog(shader, maximumLength, &messageLength, message);
	}

	return message;
}


char* Renderer::checkForShaderProgramErrors(const GLuint shader)
{
	int maximumLength = 0;
	int messageLength = 0;

	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maximumLength);

	char* message = nullptr;

	if (maximumLength > 0)
	{
		message = new GLchar[maximumLength];
		glGetShaderInfoLog(shader, maximumLength, &messageLength, message);
	}

	return message;
}


void Renderer::loadShader(const char* shaderName, const char* filePathFrag, const char* filePathVert)
{
	GLint fileSize = 0;

	// Load Vertex
	const char* vertexSource = loadFile(filePathVert, fileSize);

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexSource, &fileSize);

	glCompileShader(vertexShader);

	// Load Fragment
	const char* fragmentSource = loadFile(filePathFrag, fileSize);

	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentSource, &fileSize);

	glCompileShader(fragmentShader);

	GLuint shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	glBindAttribLocation(shaderProgram, 0, "in_position");
	glBindAttribLocation(shaderProgram, 1, "in_colour");
	glBindAttribLocation(shaderProgram, 2, "in_normal");
	glBindAttribLocation(shaderProgram, 3, "in_texCoord");

	glBindFragDataLocation(shaderProgram, 0, "out_Colour");

	glLinkProgram(shaderProgram);

	// Check shaders and program were compiled correctly, clean up if not
	char* vertexShaderStatus = checkForShaderErrors(vertexShader);
	if (vertexShaderStatus == nullptr)
	{
        std::string message = "Compiled vertex shader: ";
        message += filePathVert;
        message += " as shader #";
        message += std::to_string((unsigned int)vertexShader);
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);
	}
	else
	{
        std::string message = "Vertex shader #";
        message += std::to_string((unsigned int)vertexShader);
        message += " - ";
        message += filePathVert;
        message += " not compiled. Message: ";
        message += vertexShaderStatus;
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);
        
        glDeleteShader(vertexShader);
	}

	char* fragmentShaderStatus = checkForShaderErrors(fragmentShader);
	if (fragmentShaderStatus == nullptr)
	{
        std::string message = "Compiled fragment shader: ";
        message += filePathFrag;
        message += " as shader #";
        message += std::to_string((unsigned int)fragmentShader);
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);
	}
	else
	{
        std::string message = "Fragment shader #";
        message += std::to_string((unsigned int)fragmentShader);
        message += " - ";
        message += filePathFrag;
        message += " not compiled. Message: ";
        message += fragmentShaderStatus;
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);
        
		glDeleteShader(fragmentShader);
	}

	const char* shaderProgramStatus = checkForShaderProgramErrors(shaderProgram);
	if (shaderProgramStatus == nullptr)
	{
        std::string message = "Created shader program #";
        message += std::to_string((unsigned int)shaderProgram);
        message += " '";
        message += shaderName;
        message += "' using shaders #";
        message += std::to_string((unsigned int)vertexShader);
        message += " and #";
        message += std::to_string((unsigned int)fragmentShader);
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);

		m_shaderMap[filePathVert] = vertexShader;
		m_shaderMap[filePathFrag] = fragmentShader;
		m_shaderProgramMap[shaderName] = shaderProgram;
	}
	else
	{
        std::string message = "Could not create shader program: ";
        message += shaderName;
        
        UtilitiesManager::getInstance().getMessageLog()->logString(message);
        
		glDeleteProgram(shaderProgram);
	}

	// Delete the buffers for shader messages if they were filled
	if (vertexShaderStatus != nullptr)
	{
		delete[] vertexShaderStatus;
	}

	if (fragmentShaderStatus != nullptr)
	{
		delete[] fragmentShaderStatus;
	}

	if (vertexShaderStatus != nullptr)
	{
		delete[] shaderProgramStatus;
	}

	// come back and check all cases for these deletes, there is possible leaks

	delete[] vertexSource;
	delete[] fragmentSource;
}


char* Renderer::loadFile(const char* filePath, GLint& fileSize)
{
	std::ifstream fileStream(filePath, std::ifstream::binary);

	if (fileStream)
	{
		// get length of file:
		fileStream.seekg(0, fileStream.end);
		long long size = fileStream.tellg();
		fileSize = (GLint)size;
		fileStream.seekg(0, fileStream.beg);

		char* buffer = new char[fileSize];

		// read data as a block:
		fileStream.read(buffer, fileSize);

		fileStream.close();

        UtilitiesManager::getInstance().getMessageLog()->logString(std::string("Loaded file: ") + filePath);

		return buffer;
	}
	else
	{
        UtilitiesManager::getInstance().getMessageLog()->logString(std::string("Failed to load file: ") + filePath);
	}

	return nullptr;
}


void Renderer::loadModel(std::string name, std::string filePath)
{
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(filePath, aiProcess_ImproveCacheLocality | aiProcess_FlipUVs);

	if (!scene)
	{
        UtilitiesManager::getInstance().getMessageLog()->logString("Error loading texture '" + name + "' from '" + filePath + "' - " + importer.GetErrorString());
    }
    else
    {
        UtilitiesManager::getInstance().getMessageLog()->logString("Loaded model '" + name +"' from '" + filePath + "'.");
    }

	std::vector<GLuint> meshIndexCountVector;
	std::vector<GLuint> VAOVector;
	std::vector<GLuint*> VBOArrayVector;

	unsigned int numberOfMeshes = scene->mNumMeshes;
	for (unsigned int index = 0; index < numberOfMeshes; ++index)
	{
		GLuint VAO;
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);

		GLuint VBOlist[4];
		glGenBuffers(4, VBOlist);

		// vertices
		glBindBuffer(GL_ARRAY_BUFFER, VBOlist[0]);
		glBufferData(GL_ARRAY_BUFFER, 3 * scene->mMeshes[index]->mNumVertices * sizeof(GL_FLOAT), scene->mMeshes[index]->mVertices, GL_STATIC_DRAW); // 3*3 represents 3 vertices with 3 floats each XYZ
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		// normals
		if (scene->mMeshes[index]->HasNormals() == true)
		{
			glBindBuffer(GL_ARRAY_BUFFER, VBOlist[1]);
			glBufferData(GL_ARRAY_BUFFER, 3 * scene->mMeshes[index]->mNumVertices * sizeof(GLfloat), scene->mMeshes[index]->mNormals, GL_STATIC_DRAW);
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(2);
		}

		// UV coords
		if (scene->mMeshes[index]->HasTextureCoords(0) == true)
		{
			glBindBuffer(GL_ARRAY_BUFFER, VBOlist[2]);
			glBufferData(GL_ARRAY_BUFFER, 3 * scene->mMeshes[index]->mNumVertices * sizeof(GLfloat), scene->mMeshes[index]->mTextureCoords[0], GL_STATIC_DRAW);
			glEnableVertexAttribArray(3); glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		}

		std::vector<GLuint> faceArray;

		for (GLuint i = 0; i < scene->mMeshes[index]->mNumFaces; ++i)
		{
			const aiFace face = scene->mMeshes[index]->mFaces[i];
			faceArray.push_back(face.mIndices[0]);
			faceArray.push_back(face.mIndices[1]);
			faceArray.push_back(face.mIndices[2]);
		}

		// todo could use this to get rid of meshindexcountvector?
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOlist[3]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, scene->mMeshes[index]->mFaces->mNumIndices * scene->mMeshes[index]->mNumFaces * sizeof(GLuint), faceArray.data(), GL_STATIC_DRAW);

		//glDeleteBuffers(4, VBOlist);

		glBindVertexArray(0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		meshIndexCountVector.push_back(scene->mMeshes[index]->mNumFaces * scene->mMeshes[index]->mFaces->mNumIndices);

		VAOVector.push_back(VAO);
		VBOArrayVector.push_back(VBOlist);
	}

	m_modelMap[name] = Model(name, filePath, meshIndexCountVector, VAOVector, numberOfMeshes, VBOArrayVector);
}


Model Renderer::getModel(std::string name)
{
	return m_modelMap[name];
}


void Renderer::loadTexture(std::string name, std::string filePath)
{
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID

	SDL_Surface *tmpSurface = IMG_Load(filePath.c_str());
    
	if (!tmpSurface)
    {
        UtilitiesManager::getInstance().getMessageLog()->logString("Error loading texture '" + name + "' from '" + filePath + "'.");
	}
    else
    {
        UtilitiesManager::getInstance().getMessageLog()->logString("Loaded texture '" + name + "' from path '" + filePath + "'.");
    }

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	SDL_PixelFormat *format = tmpSurface->format;

	GLuint externalFormat, internalFormat;
	if (format->Amask)
	{
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format->Bmask) ? GL_RGBA : GL_BGRA;
	}
	else
	{
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format->Bmask) ? GL_RGB : GL_BGR;
	}

	// todo always been using internalFormat then externalFormat in below line, causes lines over textures on mac, using GL_RGBA has fixed the bug
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, tmpSurface->w, tmpSurface->h, 0, externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);

	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer

	m_textureMap[name] = texID;
}


void Renderer::loadCubeMap(const char *fname[6])
{
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID
	GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y };

	SDL_Surface *tmpSurface;

	glBindTexture(GL_TEXTURE_CUBE_MAP, texID); // bind texture and set parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	for (int i = 0; i < 6; i++)
	{
		// load file - using core SDL library
		tmpSurface = IMG_Load(fname[i]);
        
		if (!tmpSurface)
		{
            UtilitiesManager::getInstance().getMessageLog()->logString("Error loading cubemap texture.");
		}
        else
        {
            if (i == 5)
            {
                 UtilitiesManager::getInstance().getMessageLog()->logString("Loaded cubemap texture.");
            }
        }
        
		SDL_PixelFormat *format = tmpSurface->format;
		GLuint externalFormat, internalFormat;
		if (format->Amask)
		{
			internalFormat = GL_RGBA;
			externalFormat = (format->Rmask < format->Bmask) ? GL_RGBA : GL_BGRA;
		}
		else
		{
			internalFormat = GL_RGB;
			externalFormat = (format->Rmask < format->Bmask) ? GL_RGB : GL_BGR;
		}

        // added A to formats for osx
		glTexImage2D(sides[i], 0, GL_RGB, tmpSurface->w, tmpSurface->h, 0, GL_RGB, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		//texture loaded, free the temporary buffer
		SDL_FreeSurface(tmpSurface);
	}
    
	m_textureMap["skyBox"] = texID;
}


GLuint Renderer::getTextureOGL(std::string name)
{
	return m_textureMap[name];
}


void Renderer::useTexture2D(std::string name)
{
	// todo check if texture exists?
	glBindTexture(GL_TEXTURE_2D, m_textureMap[name]);
}
