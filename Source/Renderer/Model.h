#pragma once

#include <vector>
#include <string>
#include <iostream>

#ifdef _WIN32
#include <GL/glew.h>
#elif __APPLE__
#include <OpenGL/gl3.h>
#endif

class Model
{
public:
	Model() {}
	Model(std::string name, std::string filePath, std::vector<GLuint>& meshIndexCountVector, std::vector<GLuint>& VAOVector, unsigned int numberOfMeshes, std::vector<GLuint*> VBOListArrayVector);
	~Model();

public:
	unsigned int getNumberOfMeshes();
	std::vector<GLuint>& getMeshIndexCountVector();
	std::vector<GLuint>& getVAOVector();

private:
	std::string m_name;
	std::string m_filePath;

	unsigned int m_numberOfMeshes;
	std::vector<GLuint> m_meshIndexCountVector;
	std::vector<GLuint> m_VAOVector;
	std::vector<GLuint*> m_VBOArrayVector;
};

