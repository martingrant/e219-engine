#pragma once

#define DEG_TO_RADIAN 0.017453293

#include <stack>
#include <sstream> 
#include <vector>
#include <memory>
#include <unordered_map>

#ifdef _WIN32
#include <SDL_image.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../Dirent/dirent.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#elif __APPLE__
#include <SDL2_image/SDL_image.h>
#include <OpenGL/gl3.h>
#include "glm.hpp"
#include "matrix_transform.hpp"
#include "type_ptr.hpp"
#include <dirent.h>
#include "Importer.hpp"
#include "scene.h"
#include "postprocess.h"
#endif

#include "../Utilities/UtilitiesManager.h"
#include "../Camera/Camera.h"
#include "../Camera/FirstPersonCamera.h"
#include "../Camera/FreeCamera.h"
#include "Model.h"
#include "../Entities/Entity.h"
#include "../Entities/Components/Component.h"
#include "../Entities/Components/PhysicalComponent.h"
#include "../Entities/Components/ModelComponent.h"
#include "../Entities/Components/TextureComponent.h"

#include "Lights/Light.h"

class Renderer
{
	/* Class Constructors & Destructors */
public:
	Renderer(unsigned int windowWidth, unsigned int windowHeight);
	~Renderer();

	void renderToTexture();
	GLuint FramebufferName;
	GLuint renderedTexture;
	void renderToScreen();


	void renderDepth();
	GLuint depthBuffer;
	GLuint depthTexture;


	int selectedLight;
	Light lightList[2];
	glm::vec4 lightPositions[2];
	glm::vec3 lightConeDirection[2];

	GLfloat lightpos[3];

public:
	void setUniform(GLuint shader, Light iLightList[]);	// used for passing light array to shader
    void setUniform3f(GLuint shader, std::string uniform, const GLfloat* data);
	void setUniform4f(GLuint shader, std::string uniform, const GLfloat* data);
	void setUniformMatrix3fv(const GLuint program, const char* uniformName, const GLfloat *data);
	void setUniformMatrix4fv(const GLuint program, const char* uniformName, const GLfloat *data);
	void drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive);

    void renderBegin();
    void renderEnd();
	void renderEntity(Entity* entity);
	void renderProp(std::string modelName, std::string textureName, float xPosition, float yPosition, float zPosition, float xScale, float yScale, float zScale);
    void renderPropLight(std::string modelName, std::string textureName, float xPosition, float yPosition, float zPosition, float xScale, float yScale, float zScale);
	void renderSkybox();
    void renderCrosshair();
	void renderTexture(std::string name, float positionX, float positionY, unsigned int width, unsigned int height);

	void setProjectionMatrix(float fieldOfView, float aspectX, float aspectY, float nearPlane, float farPlane); // todo should go in camera?

private:
	glm::mat4 m_projectionMatrix;
    
    std::stack<glm::mat4> m_stack;
    glm::mat4 m_modelview;

    /* Camera */
public:
    void updateCamera();
	void setCameraTarget(Entity* entity);
	void setCamera(CameraType cameraType);
    
private:
	Camera* m_camera;
    FirstPersonCamera* m_firstPersonCamera;
	FreeCamera* m_freeCamera;
    
	/* Shaders */
public:
	void useShaderProgram(std::string shaderName);
	void loadShaders(const char* directoryPath);

private:
	char* checkForShaderErrors(const GLuint shader);
	char* checkForShaderProgramErrors(const GLuint shader);
	void loadShader(const char* shaderName, const char* filePathFrag, const char* filePathVert);
	char* loadFile(const char* filePath, GLint& fileSize);

private:
	std::unordered_map<std::string, GLuint> m_shaderProgramMap;
	std::unordered_map<std::string, GLuint> m_shaderMap;

	/* Models */
public:
	void loadModel(std::string name, std::string filePath);
	Model getModel(std::string name);

private:
	std::unordered_map<std::string, Model> m_modelMap;

	/* Textures */
public:
	void loadTexture(std::string name, std::string filePath);
	void loadCubeMap(const char *fname[6]);
	GLuint getTextureOGL(std::string name);
	void useTexture2D(std::string name);

private:
	std::unordered_map<std::string, GLuint> m_textureMap;
};

