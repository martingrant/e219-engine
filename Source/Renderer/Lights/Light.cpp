#include "Light.h"

Light::Light()
{
}


Light::Light(glm::vec4 ambient, glm::vec4 diffuse, glm::vec4 specular, glm::vec4 position, glm::vec3 coneDirection, GLfloat coneFallOff, glm::vec3 attenuation, int active, int type)
	: m_ambient(ambient), m_diffuse(diffuse), m_specular(specular), m_position(position), m_coneDirection(coneDirection), m_coneFallOff(coneFallOff), m_attenuation(attenuation), m_active(active), m_type(type)
{
}


Light::~Light(void)
{
}


glm::vec4 Light::getAmbient()
{
	return m_ambient;
}


glm::vec4 Light::getDiffuse()
{
	return m_diffuse;
}


glm::vec4 Light::getSpecular()
{
	return m_specular;
}


glm::vec4 Light::getPosition()
{
	return m_position;
}


void Light::setPositionX(GLfloat newPosition)
{
	m_position.x = newPosition;
}


void Light::setPositionY(GLfloat newPosition)
{
	m_position.y = newPosition;
}


void Light::setPositionZ(GLfloat newPosition)
{
	m_position.z = newPosition;
}


void Light::setPositionW(GLfloat newPosition)
{
	m_position.w = newPosition;
}


glm::vec3 Light::getConeDirection()
{
	return m_coneDirection;
}


void Light::setConeDirectionX(GLfloat newDirection)
{
	m_coneDirection.x = newDirection;
}


void Light::setConeDirectionY(GLfloat newDirection)
{
	m_coneDirection.y = newDirection;
}


void Light::setConeDirectionZ(GLfloat newDirection)
{
	m_coneDirection.z = newDirection;
}


GLfloat Light::getConeFallOff()
{
	return m_coneFallOff;
}


void Light::setConeFallOff(GLfloat newConeFallOff)
{
	m_coneFallOff = newConeFallOff;
}


glm::vec3 Light::getAttenuation()
{
	return m_attenuation;
}


void Light::setAttenuationLinear(GLfloat newAttenuation)
{
	m_attenuation.y = newAttenuation;
}


void Light::setAttenuationQuadratic(GLfloat newAttenuation)
{
	m_attenuation.z = newAttenuation;
}


int Light::getStatus()
{
	return m_active;
}


void Light::turnOff()
{
	m_active = 0;
}

void Light::turnOn()
{
	m_active = 1;
}


int Light::getType()
{
	return m_type;
}


void Light::setType(int newType)
{
	m_type = newType;

	if(m_type == 2) m_position[3] = 0.0f;
	else m_position[3] = 1.0f;
}


