#include "Model.h"


Model::Model(std::string name, std::string filePath, std::vector<GLuint>& meshIndexCountVector, std::vector<GLuint>& VAOVector, unsigned int numberOfMeshes, std::vector<GLuint*> VBOListArrayVector) : m_name(name), m_filePath(filePath),
m_numberOfMeshes(numberOfMeshes), m_meshIndexCountVector(meshIndexCountVector), m_VAOVector(VAOVector), m_VBOArrayVector(VBOListArrayVector)
{
}


Model::~Model()
{
	// todo clean up for these things also being done in renderer, could cause bug when deleting?
    m_meshIndexCountVector.clear();
	m_VAOVector.clear();
}


unsigned int Model::getNumberOfMeshes()
{
	return m_numberOfMeshes;
}


std::vector<GLuint>& Model::getMeshIndexCountVector()
{
	return m_meshIndexCountVector;
}


std::vector<GLuint>& Model::getVAOVector()
{
	return m_VAOVector;
}
