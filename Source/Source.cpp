#include "Application/Application.h"

int main(int argc, char *args[])
{
	Application* honoursProject = new Application();

	while (honoursProject->run())
		continue;

    delete honoursProject;
    
	return 0;
}