#include "Application.h"

#pragma region Class Constructors & Destructors
/*
 * Constructs the Application class. Sets m_running to true and instantiates m_UtiltiesManager.
 */
Application::Application()
{
    /* Register new MessageLog with UtilitesManager */
    UtilitiesManager::getInstance().registerService(std::shared_ptr<MessageLog>(new MessageLog(true)));
    
    UtilitiesManager::getInstance().registerService(std::shared_ptr<ConfigLoader>(new ConfigLoader("../Config/config.txt")));
    
    UtilitiesManager::getInstance().getMessageLog()->logString("System constructing: Application");
    
	m_running = true;
    
    /* Register new SDLInput with UtiltiesManager */
    m_inputService = std::shared_ptr<SDLInput>(new SDLInput(4));
    UtilitiesManager::getInstance().registerService(m_inputService);

	/* Create, initialise and register ScreenManager with UtiltiesManager */
	m_windowManager = std::shared_ptr<WindowManager>(new SDLWindowManager());
	UtilitiesManager::getInstance().registerService(m_windowManager);
    
	/* Register new TimerManager with UtiltiesManager */
	UtilitiesManager::getInstance().registerService(std::shared_ptr<TimeManager>(new TimeManager(30, 5)));
    
    UtilitiesManager::getInstance().getMessageLog()->logString("System constucted: Application");
}

/*
 * Destructs the Application class.
 */
Application::~Application()
{
	UtilitiesManager::getInstance().getMessageLog()->logString("System destructed: Application.");
}

#pragma endregion


#pragma region General Public Methods
/*
 * Application loop. Loop will run whilst m_running is set to true, and will stop once m_running has been set to false.
 * Checks for SDL events and updates m_screenManager.
 *
 * @return m_running - Returns a boolean variable which signifys if the application loop is currently running or not.
 */
bool Application::run()
{
    m_inputService->update();
    m_running = m_windowManager->update();
    m_windowManager->render();
    
	return m_running;
}

#pragma endregion