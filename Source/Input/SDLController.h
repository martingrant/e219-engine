#pragma once

#include <iostream>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
#include <SDL2/SDL.h>
#endif


class SDLController
{
public:
	/* Class Constructor & Destructor */
	SDLController(unsigned int controllerID);
	~SDLController(void);

public:
	/* Manage Controller Device */
	void setControllerID(int controllerID);
	bool openController();
	bool closeController();
	bool getControllerStatus();
	int getControllerID() { return m_controllerID; }

	/* Controller data methods */
	int getControllerAxis(const char* axis);
	bool getControllerButtonState(const char* button);

private:
	int m_controllerID;
	SDL_GameController* m_controller;
	SDL_Haptic* m_haptic;

	bool m_controllerStatus;

	bool openHaptic();
	bool closeHaptic();
};

