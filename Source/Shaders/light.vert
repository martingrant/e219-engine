// textured.vert
// use textures, but no lighting
#version 330

in vec3 in_position;
in vec3 in_normal;

out vec3 lightIntensity;

uniform vec4 lightPosition;
uniform vec3 diffuse;
uniform vec3 intensity;

uniform mat4 modelview;
uniform mat3 normalMatrix;
uniform mat4 projection;
//uniform mat4 MVP;

// multiply each vertex position by the MVP matrix
void main(void) {

	// vertex into eye coordinates
	//vec4 vertexPosition = model * vec4(in_position, 1.0);
	//gl_Position = vertexPosition;

    vec3 tnorm = normalize(normalMatrix * in_normal);
    vec4 eyeCoords = modelview * vec4(in_position, 1.0);
    vec3 s = normalize(vec3(lightPosition - eyeCoords));
    
    // diffuse shading equation
    lightIntensity = diffuse * intensity * max(dot(s, tnorm), 0.0);
    
    mat4 MVP = modelview * projection;
    
    // convert position to clip coordinates and pass along
    gl_Position = MVP * vec4(in_position, 1.0);
    
    

}