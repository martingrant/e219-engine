#version 330 core
 
// Input vertex data, different for all executions of this shader.
in  vec3 in_position;
 


// Used for shadow lookup
	varying vec4 ShadowCoord;
	
	void main()
	{
		ShadowCoord= gl_TextureMatrix[7] * gl_Vertex;
	  
		gl_Position = ftransform();
	
		gl_FrontColor = gl_Color;
	}