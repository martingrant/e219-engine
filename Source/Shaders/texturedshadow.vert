// textured.vert
// use textures, but no lighting
#version 330

uniform mat4 modelview;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 DepthBiasMVP;

in vec3 in_position;

in vec3 in_Normal;


in vec2 in_texCoord;
out vec2 ex_TexCoord;

out vec4 ShadowCoord;

// multiply each vertex position by the MVP matrix
void main(void) {




	gl_Position = projection * modelview * vec4(in_position,1.0);

	ex_TexCoord = in_texCoord;

}