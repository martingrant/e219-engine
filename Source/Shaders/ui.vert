// textured.vert
// use textures, but no lighting
#version 330

uniform mat4 model;

in vec3 in_position;
//in vec3 in_Colour; // colour not used with lighting
//in vec3 in_Normal;
//out vec4 ex_Color;

in vec2 in_texCoord;
out vec2 ex_TexCoord;

// multiply each vertex position by the MVP matrix
void main(void) {

	// vertex into eye coordinates
	vec4 vertexPosition = model * vec4(in_position, 1.0);
	gl_Position = vertexPosition;

	ex_TexCoord = in_texCoord;

}