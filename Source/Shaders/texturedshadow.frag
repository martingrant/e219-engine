// textured.frag
#version 330

// Some drivers require the following
precision highp float;

uniform sampler2D textureUnit0;
uniform sampler2DShadow shadowMap;

in vec2 ex_TexCoord;
in vec4 ShadowCoord;

layout(location = 0) out vec3 color;

void main(void) 
{
		// Light emission properties
	vec3 LightColor = vec3(1,1,1);
	float LightPower = 1.0f;
	
	// Material properties
	vec3 MaterialDiffuseColor = texture2D(textureUnit0, ex_TexCoord ).rgb;
	vec3 MaterialAmbientColor = vec3(0.1,0.1,0.1) * MaterialDiffuseColor;
	vec3 MaterialSpecularColor = vec3(0.3,0.3,0.3);

	float visibility = 1.0;



if ( texture( shadowMap, ShadowCoord.xy ).z  <  ShadowCoord.z){
    visibility = 0.5;
}




	color = MaterialAmbientColor + visibility * MaterialDiffuseColor * LightColor * LightPower  + visibility * MaterialSpecularColor * LightColor * LightPower ;
}