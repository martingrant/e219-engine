// mulitplelight.frag matching multiple light.vert
// Vertex shader for calculating multiple phong-shaded light sources
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec3 coneDirection;
	float coneFallOff;
	vec3 attenuation;
	int IsActive;
	int type;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

const int NUMLIGHTS = 2;
uniform lightStruct lights[NUMLIGHTS];
//uniform materialStruct material;
uniform sampler2D textureUnit0;

in vec3 ex_Normal;
in vec3 ex_View;
in vec3 ex_Position;
in vec2 ex_TexCoord;

layout(location = 0) out vec4 out_Colour;

vec3 lightDirection;
vec4 combinedColour;

materialStruct material;


vec4 addDS(lightStruct light, vec4 addColour)
{
	// Add diffuse
	vec4 diffuseI = light.diffuse * material.diffuse;
	addColour += diffuseI * max(dot(normalize(ex_Normal), normalize(lightDirection)), 0);

	// Calculate reflection of light
	vec3 lightReflection = normalize(reflect(normalize(-lightDirection), normalize(ex_Normal)));

	// Add specular
	vec4 specularI = light.specular * material.specular;
	addColour += specularI * pow(max(dot(lightReflection, lightDirection), 0), material.shininess);

	return vec4(addColour.rgb, 1.0);
}

vec4 combineLights(lightStruct light) 
{
	// Calculate light distance
	float lightDistance = distance(light.position.xyz, ex_Position);

	// Calculate attenuation
	float ex_attenuation = light.attenuation.x + light.attenuation.y * lightDistance + light.attenuation.z * lightDistance*lightDistance;

	// Calculate light direction
	if(light.type == 2) 
	{
		// Directional light
		lightDirection = vec3(normalize(light.position));
	}
	else 
	{
		// Point and spot
		lightDirection = normalize(light.position.xyz - ex_Position.xyz);	
	}

	// Add ambient
	vec4 componentColour = light.ambient * material.ambient;

	if(light.type == 0)
	{
		if((dot(lightDirection, -light.coneDirection.xyz)) > light.coneFallOff)
		{
			componentColour = addDS(light, componentColour);
		}
		else
		{
			combinedColour = vec4(0.0f, 0.0f, 0.0f, 1.0f);
		}
	} 
	else
	{
		componentColour = addDS(light, componentColour);
	}

	// Add attenuation
	combinedColour = vec4(componentColour.rgb / ex_attenuation, 1.0);

	return min(combinedColour, vec4(1.0));
}



void main(void) 
{
	material.ambient = vec4(0.2, 0.4, 0.2, 1.0);
	material.diffuse = vec4(0.5, 1.0, 0.5, 1.0);
	material.specular = vec4(0.0, 0.1, 0.0, 1.0);
	material.shininess = 2.0;
	

	// Iterate over and combine lights
	for(int i=0; i < NUMLIGHTS; i++) 
	{
		if(lights[i].IsActive == 1) 
		{
			out_Colour += combineLights(lights[i]);
		}
	}

	// Fragment colour
	out_Colour = min(out_Colour, vec4(1.0)) * texture(textureUnit0, ex_TexCoord);
	//out_Colour = vec4(0.8, 0.5, 0.1, 1.0);
}