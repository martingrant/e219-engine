// mulitplelight.vert matching multiplelight.frag
// Vertex shader for calculating multiple phong-shaded light sources
#version 330

uniform mat4 modelview;
uniform mat4 projection;
uniform mat3 normalMatrix;

in  vec3 in_position;
in  vec3 in_normal;
out vec3 ex_Normal;
out vec3 ex_View;
out vec3 ex_Position;

in vec2 in_texCoord;
out vec2 ex_TexCoord;

void main(void) {

	// vertex into eye coordinates
	ex_Position = vec3(modelview * vec4(in_position,1.0));

	// Find View - in eye coordinates, eye is at (0,0,0)
	ex_View = normalize(-ex_Position).xyz;

	// surface normal in eye coordinates
	//mat3 normalmatrix = transpose(inverse(mat3(modelview)));
	ex_Normal = normalize(normalMatrix * in_normal);

	ex_TexCoord = in_texCoord;

    gl_Position = projection * vec4(ex_Position, 1.0);	
}