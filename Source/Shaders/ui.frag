// textured.frag
#version 330

// Some drivers require the following
precision highp float;

uniform sampler2D textureUnit0;

in vec2 ex_TexCoord;
layout(location = 0) out vec4 out_Colour;

void main(void) 
{
	// Fragment colour
	out_Colour = texture(textureUnit0, ex_TexCoord);
}