// textured.frag
#version 330

// Some drivers require the following
precision highp float;

in vec3 lightIntensity;

layout(location = 0) out vec4 out_Colour;

void main(void) 
{
	// Fragment colour
    // need to times light position by rendering stack/camera i think
	//out_Colour = vec4(lightIntensity, 1.0);
    out_Colour = vec4(1.0, 0.5, 0.5, 1.0);
}